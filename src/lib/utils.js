export function getFileExtension(filename) {
  var ext = /^.+\.([^.]+)$/.exec(filename);
  return ext == null ? "" : ext[1];
}

export function isVideoLink(path) {
  const extension = getFileExtension(path);
  const vidFormats = ['webm', 'mp4', 'mpeg', 'avi', 'mov'];

  if (vidFormats.includes(extension)) return true;

  else return false;
}
